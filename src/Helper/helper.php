<?php
if(!function_exists('encrypt'))
{
    function encrypt(string $string, string $encryptionKey)
    {
        $class = new \Djunehor\EncDec\EncryptDecrypt($string, $encryptionKey);

        return $class->encrypt();
    }
}

if(!function_exists('decrypt'))
{
    function decrypt(string $string, string $encryptionKey)
    {
        $class = new \Djunehor\EncDec\EncryptDecrypt($string, $encryptionKey);

        return $class->decrypt();
    }
}