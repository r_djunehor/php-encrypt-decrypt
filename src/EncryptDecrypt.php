<?php

namespace Djunehor\EncDec;


class EncryptDecrypt
{
    private $string;
    private $encryptionKey;

    /**
     * Encrypt constructor.
     * @param string $string
     * @param string $encryptionKey
     * @throws \Exception
     */
    public function __construct(string $string, string $encryptionKey)
    {
        //throwing exception so as to short circuit i.e early return
        if(empty($encryptionKey)) {
            throw new \Exception('Encryption key cannot be empty');
        }
        elseif(!is_numeric($string)) {
            throw new \Exception('String must contain only numbers');
        }

        $this->string = $string;
        $this->encryptionKey = $encryptionKey;
    }

    public function encrypt() : string
    {
        $encryptedText = '';

        //split string into characters array
        $splitString = str_split($this->string);
        $splitKey = str_split($this->encryptionKey);


        foreach ($splitString as $key => $value)
        {
            //For each character in the textToEncrypt string, take its position (0 being the first character in the string) and
            //add that to the ASCII character value for the corresponding character in the encryptionKey (wrapping to the
            //start of the key if textToEncrypt is longer than encryptionKey). Call this value n.
            $n = $key + (ord($splitKey[$key] ?? $splitKey[0]));

            //Calculate the nth Prime number (see below). Sum the Prime number, the original character position and the
            //original unencrypted character value.
            $sum = $this->nthPrimeNumber($n) + $key + $value;

            // If the value is greater than 126, wrap the character back into the valid
            //range of characters (so that 127 becomes 32, 128 becomes 33, and so on…)
            if($sum > 126)
            {
                $sum = ($sum - 127) + 32;
            }

            $encryptedText .=$sum;
        }

        return $encryptedText;
    }

    public function decrypt(): string
    {
        $decryptedText = '';
        $splitEncryptedText = str_split($this->string, 3);
        $splitKey = str_split($this->encryptionKey);

        foreach ($splitEncryptedText as $key => $sum)
        {
            //we have sum, to get value, we convert subject of formula

            $n = $key + ord($splitKey[$key] ?? $splitKey[0]);
            $value = $sum - ($key + $this->nthPrimeNumber($n));

            if($value < 126)
            {
                $value = ($value + 127) - 32;
            }
            $decryptedText .= $value;
        }

        return $decryptedText;
    }

    public function nthPrimeNumber($n)
    {
        $i = 0;
        $j = 0;
        while($i != $n)
        {
            $j++;
            if($this->isPrime($j))
            {
                $i++;
            }
        }

        return $j;
    }

    public function isPrime($num) {
        //1 is not prime. See: http://en.wikipedia.org/wiki/Prime_number#Primality_of_one
        if($num == 1)
            return false;

        //2 is prime (the only even number that is prime)
        if($num == 2)
            return true;

        /**
         * if the number is divisible by two, then it's not prime and it's no longer
         * needed to check other even numbers
         */
        if($num % 2 == 0) {
            return false;
        }

        /**
         * Checks the odd numbers. If any of them is a factor, then it returns false.
         * The sqrt can be an aproximation, hence just for the sake of
         * security, one rounds it to the next highest integer value.
         */
        $ceil = ceil(sqrt($num));
        for($i = 3; $i <= $ceil; $i = $i + 2) {
            if($num % $i == 0)
                return false;
        }

        return true;
    }
}
