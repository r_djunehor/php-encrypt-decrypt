## Test
- Clone the repo `git clone https://github.com/djunehor/php-encrypt-decrypt`
- Enter folder `cd php-encrypt-decrypt`
- Install dependencies `composer install`
- Run tests `composer test`

## Usage
### Via Class
```php
use Djunehor\EncDec\EncryptDecrypt;

$class = new \Djunehor\EncDec\EncryptDecrypt($string, $encryptionKey);

//to encrypt supplied string
$encryptedText = $class->encrypt();

//to decrypt supplied string
$decryptedText = $class->decrypt();
```

### Via Helpers
```php
//to encrypt
$encryptedText = encrypt($string, $encryptionKey);

//to decrypt
$decryptedText = decrypt($string, $encryptionKey);
```