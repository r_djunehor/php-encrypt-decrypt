<?php

namespace Djunehor\EncDec\Test;

use Djunehor\EncDec\EncryptDecrypt;
use PHPUnit\Framework\TestCase;

class EncryptDecryptTest extends TestCase
{
    private $string = '81298102910871';
    private $encryptionKey = 'hgg5877';
    private $encryptedText = '482476480180198192194521535532534555565562';

    public function testExceptionOnEmptyEncryptionKey()
    {
        $this->expectException(\Exception::class);
        new EncryptDecrypt($this->string, '');

    }

    public function testExceptionOnNonNumericString()
    {
        $this->expectException(\Exception::class);
        new EncryptDecrypt('97un77b', $this->encryptedText);

    }

    public function testEncrypt()
    {
        $class = new EncryptDecrypt($this->string, $this->encryptionKey);

        $encryptedText = $class->encrypt();

        $this->assertEquals($this->encryptedText, $encryptedText);

    }

    public function testDecrypt()
    {
        $class = new EncryptDecrypt($this->encryptedText, $this->encryptionKey);

        $decryptedText = $class->decrypt();

        $this->assertEquals($this->string, $decryptedText);

    }

    public function testEncryptWithHelper()
    {
        $encryptedText = encrypt($this->string, $this->encryptionKey);

        $this->assertEquals($this->encryptedText, $encryptedText);

    }

    public function testDecryptWithHelper()
    {
        $decryptedText = decrypt($this->encryptedText, $this->encryptionKey);

        $this->assertEquals($this->string, $decryptedText);

    }
}

