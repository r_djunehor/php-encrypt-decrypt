<?php
include_once 'vendor/autoload.php';
use Djunehor\EncDec\EncryptDecrypt;

$string = rand(111111111, 999999999);
$encryptionKey = md5(time());

echo "Text is => $string\n";

$class = new EncryptDecrypt($string, $encryptionKey);
$encryptedText = $class->encrypt();
echo "Encrypted Text is => $encryptedText\n";

$class = new EncryptDecrypt($encryptedText, $encryptionKey);
$decryptedText = $class->decrypt();
echo "Decrypted Text is => $decryptedText\n";