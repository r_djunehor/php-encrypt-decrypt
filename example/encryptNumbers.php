<?php
include_once 'vendor/autoload.php';
use Djunehor\EncDec\EncryptDecrypt;

$string = rand(111111111, 999999999);
$encryptionKey = md5(time());

$class = new EncryptDecrypt($string, $encryptionKey);
$encryptedText = $class->encrypt();
echo "Encrypted Text is => $encryptedText\n";